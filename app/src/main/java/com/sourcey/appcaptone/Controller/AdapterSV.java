package com.sourcey.appcaptone.Controller;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sourcey.appcaptone.Model.Student;
import com.sourcey.appcaptone.Model.Student_History;
import com.sourcey.appcaptone.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterSV extends ArrayAdapter<Student_History> {
    Context context;
    int resource;
    // List<Class> objects;

    List<Student_History> objects;
    public AdapterSV(@NonNull Context context, int resource, @NonNull List<Student_History> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }
    private class ViewHolder{

    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(resource,parent,false);

        //ImageView anh =(ImageView) view.findViewById(R.id.anh) ;
        TextView ten= (TextView) view.findViewById(R.id.ten);
        TextView id = (TextView) view.findViewById(R.id.idSV);

        Student_History sv =objects.get(position);
//        String url = sv.getUrl();
//        Picasso.with(context).load(url).into(anh);


        ten.setText(sv.getName());
        id.setText(sv.getId());

        return view;

    }



}

