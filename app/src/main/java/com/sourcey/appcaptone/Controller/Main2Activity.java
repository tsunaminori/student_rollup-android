package com.sourcey.appcaptone.Controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sourcey.appcaptone.Model.UserDefault;
import com.sourcey.appcaptone.R;
import com.squareup.picasso.Picasso;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();
    public  String idUserDefault;
    public  String url;
    public  String chuVu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        TextView ten ,id ,chucvu;
        ImageView avata;

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Intent intent1 = getIntent();
        String idUS = intent1.getExtras().getString("ID");
        String url1 = intent1.getExtras().getString("URL");

        MyFirebaseMessagingService my = new MyFirebaseMessagingService();
        my.onNewToken("TAM2432423423432423");

        this.idUserDefault = idUS;
        String cv = intent1.getExtras().getString("CHUCVU");
        String name = intent1.getExtras().getString("NAME");


        avata = navigationView.getHeaderView(0).findViewById(R.id.anh);
        String url = url1;
        Picasso.with(Main2Activity.this).load(url).into(avata);
        navigationView.setNavigationItemSelectedListener(this);

        id = navigationView.getHeaderView(0).findViewById(R.id.IDUser);
        id.setText(this.idUserDefault);
        ten = navigationView.getHeaderView(0).findViewById(R.id.ten);
        ten.setText(name);
        chucvu = navigationView.getHeaderView(0).findViewById(R.id.chucvu);
        chucvu.setText(cv);

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    public  void getEmail() {
//
//        myRef.child("THONGTINNGUOIDUNG").child("212121").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if (dataSnapshot.getChildrenCount() == 0){
//                    Toast.makeText(Main2Activity.this, "Load data failed.",
//                            Toast.LENGTH_SHORT).show();
//                }
//                else {
//                    String name = dataSnapshot.child("BANE").getValue().toString();
//                    login(email, _passwordText.getText().toString());
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//
//
//        });
//
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_share) {
            Intent intent = new Intent(this, AttendenActivity.class);
            startActivity(intent);

            // Handle the camera action
        }  else if (id == R.id.nav_share) {


            Intent intent = new Intent(this, PushNotification.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(),"SEND",Toast.LENGTH_LONG).show();



        } else if (id == R.id.nav_send) {
           // Intent intent = new Intent(this, SearchMainActivity.class);
            Intent intent = new Intent(Main2Activity.this, SearchMainActivity.class);
            intent.putExtra("IDUS",idUserDefault);
            startActivity(intent);
        }
        else if (id == R.id.nav_chart) {
            Intent intent = new Intent(this, Chart.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_sendsms) {
            Intent intent = new Intent(this, SendSms.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_logout) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    void delay(){
        final ProgressDialog progressDialog = new ProgressDialog(Main2Activity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading !!!");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed

                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }
}
