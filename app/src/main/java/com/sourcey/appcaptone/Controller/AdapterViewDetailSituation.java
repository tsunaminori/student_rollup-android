package com.sourcey.appcaptone.Controller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sourcey.appcaptone.Model.Student_History;
import com.sourcey.appcaptone.R;

import java.util.List;

public class AdapterViewDetailSituation extends ArrayAdapter<String> {
    Context context;
    int resource;
    // List<Class> objects;

    List<String> objects;
    public AdapterViewDetailSituation(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }
    private class ViewHolder{

    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(resource,parent,false);

        // ImageView anh =(ImageView) view.findViewById(R.id.anh) ;
        TextView date= (TextView) view.findViewById(R.id.dateDetail);

      //  List <String> sv =objects.get(position);
        //   String url = sv.getUrl();
        //    Picasso.with(context).load(url).into(anh);
        date.setText(objects.get(position).toString());

        return view;

    }



}