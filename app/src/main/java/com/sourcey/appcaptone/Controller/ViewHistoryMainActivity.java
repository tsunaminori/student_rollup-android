package com.sourcey.appcaptone.Controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sourcey.appcaptone.Model.Student;
import com.sourcey.appcaptone.Model.Student_History;
import com.sourcey.appcaptone.Model.UserDefault;
import com.sourcey.appcaptone.R;

import java.util.ArrayList;

public class ViewHistoryMainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();
    private DatabaseReference mDatabase;
    ListView listView;
    ArrayList<Student_History> arrStudents = new ArrayList<>();
    AdapterSV adapterSV;
    public  String sll;
    // ArrayAdapter<Student> arrStudents = new ArrayList<>();
    ArrayList<String> listDate = new ArrayList<String>();
    public  String maLop;
    UserDefault s;
        Spinner chonNgay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_history_main);
//        Toast.makeText(ViewHistoryMainActivity.this, "TAM Successful!.",
//                Toast.LENGTH_SHORT).show();
        Intent intent = getIntent();
        this.maLop = intent.getExtras().getString("MALOP");
        chonNgay = (Spinner)findViewById(R.id.chonNgay);

        listView = (ListView) findViewById(R.id.listlop);
        final TextView sl = (TextView) findViewById(R.id.txtSL);

        getDate();
     //   loaddataAfterClick();



        chonNgay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String tmp = chonNgay.getSelectedItem().toString();
               arrStudents.removeAll(arrStudents);
//                Toast.makeText(ViewHistoryMainActivity.this, tmp,
//                        Toast.LENGTH_LONG).show();

                final ProgressDialog progressDialog = new ProgressDialog(ViewHistoryMainActivity.this,
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Loading !!!");
                progressDialog.show();


                // TODO: Implement your own authentication logic here.

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                // On complete call either onLoginSuccess or onLoginFailed

                                // onLoginFailed();
                                progressDialog.dismiss();
                            }
                        }, 2000);

                getListStudent(tmp);
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                loadList();
                                sl.setText(sll);
                            }
                        }, 3000);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



    void loadList() {
//        if (arrStudents.size()>0){
//            arrStudents.removeAll(arrStudents);
//            adapterSV = new AdapterSV(this, R.layout.custom_history, arrStudents);
//            listView.setAdapter(adapterSV);
//        }
//         else {
        adapterSV = new AdapterSV(this, R.layout.custom_history, arrStudents);
            listView.setAdapter(adapterSV);
              sll = String.valueOf(arrStudents.size());
        listView.setDivider(new ColorDrawable(Color.parseColor("#E5E5E5")));

        // set ListView divider height
        listView.setDividerHeight(2);
     // /  }
       // }
    }
    void addCombo(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listDate);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chonNgay.setAdapter(adapter);
    }

    void getDate() {

        mDatabase = FirebaseDatabase.getInstance().getReference();
        myRef.child("DIEMDANHSINHVIEN_DIHOC").child(maLop).child("KETQUATHEONGAY").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    Toast.makeText(getBaseContext(), "No Data!", Toast.LENGTH_LONG).show();

                }
                    for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                    listDate.add(childSnapshot.getKey().toString());
                    addCombo();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    void getListStudent(String ngay){

        Intent intent = getIntent();
        String malop1 = intent.getExtras().getString("MALOP");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        myRef.child("DIEMDANHSINHVIEN_DIHOC").child(maLop).child("KETQUATHEONGAY").child(ngay).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                    final String clubkey = childSnapshot.getKey();
                    myRef.child("DIEMDANHSINHVIEN_DIHOC").child("THONGTINNGUOIDUNG").child(clubkey).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String TEN = (String) dataSnapshot.child("TEN").getValue();
                        //    String URL = (String) dataSnapshot.child("URL_HINHANH").getValue();

                            Student_History sv = new Student_History("SSSS",TEN,clubkey);
                            arrStudents.add(sv);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }


                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    void loaddataAfterClick() {

        adapterSV = new AdapterSV(this, R.layout.custom_history, arrStudents);
        listView.setAdapter(adapterSV);
    }
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
     //  String tmp = String.valueOf(adapterView.getItemIdAtPosition(i));
       // Toast.makeText(ViewHistoryMainActivity.this, "This is my Toast message!",
              //  Toast.LENGTH_LONG).show();
      //  Log.d("tmp",tmp);
//        getListStudent(tmp);
//
//        new android.os.Handler().postDelayed(
//                new Runnable() {
//                    public void run() {
//                       Log.d("mm",arrStudents.toString());
//                    }
//                }, 5000);
//        String[] arraySpinner = listDate.toArray(new String[0]);
//       // loaddataAfterClick();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
