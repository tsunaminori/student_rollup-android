package com.sourcey.appcaptone.Model;

public class Class {


    private String BATDAU;
    private String KETTHUC;
    private String PHONGHOC;

    public String getTenlop() {
        return tenlop;
    }

    public void setTenlop(String tenlop) {
        this.tenlop = tenlop;
    }

    private String tenlop;
    private  int SOLUONGSINHVIEN,SOLUONGTINCHI;
    public Class(){}
    public Class(String tenlop, String BATDAU, String KETTHUC, String PHONGHOC, int SOLUONGSINHVIEN, int SOLUONGTINCHI) {
        this.BATDAU=BATDAU;
        this.KETTHUC=KETTHUC;
        this.PHONGHOC=PHONGHOC;
        this.SOLUONGSINHVIEN=SOLUONGSINHVIEN;
        this.SOLUONGTINCHI=SOLUONGTINCHI;
        this.tenlop = tenlop;

    }
//https://o7planning.org/vi/10433/huong-dan-lap-trinh-android-voi-database-sqlite



    public void setBatdau(String BATDAU) {
        this.BATDAU = BATDAU;
    }

    public void setKethuc(String KETTHUC) {
        this.KETTHUC = KETTHUC;
    }

    public void setPhonghoc(String PHONGHOC) {
        this.PHONGHOC =PHONGHOC ;
    }

    public void setSolsv(int SOLUONGSINHVIEN) {
        this.SOLUONGSINHVIEN = SOLUONGSINHVIEN;
    }

    public void setSotc(int SOLUONGTINCHI) {
        this.SOLUONGTINCHI = SOLUONGTINCHI;
    }



    public String getBatdau() {
        return BATDAU;
    }

    public String getKethuc() {
        return KETTHUC;
    }

    public String getPhonghoc() {
        return PHONGHOC;
    }

    public int getSolsv() {
        return  SOLUONGSINHVIEN;
    }

    public int getSotc() {
        return SOLUONGTINCHI;
    }
}
