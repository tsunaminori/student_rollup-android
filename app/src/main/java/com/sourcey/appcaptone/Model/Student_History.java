package com.sourcey.appcaptone.Model;

import androidx.annotation.NonNull;

public class Student_History {
    public Student_History(String url, String name, String id) {
        Url = url;
        Name = name;
        idSV = id;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    String Url;
    String Name;

    public String getId() {
        return idSV;
    }

    public void setId(String id) {
        this.idSV = id;
    }

    String idSV;

    @NonNull
    @Override
    public String toString() {
        return this.getId() +"/" + this.getName();
    }
}
