package com.sourcey.appcaptone.Model;

public class Student_warn {
    private  String CHUCVU;
    private String QUYEN;
    private String TEN;
    private String URL_HINHANH;
    private String EMAIL;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    private String ID;

    public String getLv() {
        return lv;
    }

    public void setLv(String lv) {
        this.lv = lv;
    }

    public  String lv ;
    public String getCHUCVU() {
        return CHUCVU;
    }

    public void setCHUCVU(String CHUCVU) {
        this.CHUCVU = CHUCVU;
    }

    public String getQUYEN() {
        return QUYEN;
    }

    public void setQUYEN(String QUYEN) {
        this.QUYEN = QUYEN;
    }

    public String getTEN() {
        return TEN;
    }

    public void setTEN(String TEN) {
        this.TEN = TEN;
    }

    public String getURL_HINHANH() {
        return URL_HINHANH;
    }

    public void setURL_HINHANH(String URL_HINHANH) {
        this.URL_HINHANH = URL_HINHANH;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public Student_warn(){}

    public Student_warn(String TEN, String lv, String ID, String url){

        this.TEN=TEN;
        this.lv = lv;
        this.ID= ID;
        this.URL_HINHANH = url;
    }


}
